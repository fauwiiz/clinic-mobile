import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import { Button } from 'react-native-elements';
import Odoo from 'react-native-odoo';

const odoo = new Odoo({
    host: '192.168.100.15',
    port: 8088,
    database: 'odoo',
    username: 'admin',
    password: 'admin'
})

odoo.connect(function (err) {
    if (err) { return console.log(err); }
})

class SignUpScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            partners: [{ name: 'Tidak Connect' }]
        }
    }
    daftar() {
        var get = {
            fields: ['name', 'street', 'mobile', 'email'],
        }
        odoo.search_read('res.partner', get, function (err, partners) {
            console.log(partners);
            this.setState({
                partners: partners
            })
        }.bind(this))
    }

    masuk(){
        this.props.navigation.navigate('DrawerNavigator')
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Nama Lengkap</Text>
                <TextInput style={pickerSelectStyles.inputIOS} />
                <View style={{ paddingVertical: 5 }} />
                <Text>Alamat</Text>
                <TextInput secureTextEntry={true} password={true} style={pickerSelectStyles.inputIOS} />
                <View style={{ paddingVertical: 5 }} />
                <Text>No Telp</Text>
                <TextInput secureTextEntry={true} style={pickerSelectStyles.inputIOS} />
                <View style={{ paddingVertical: 5 }} />
                <Button onPress={this.masuk.bind(this)} containerViewStyle={{ width: '70%', paddingHorizontal: 10 }} title="Lanjut Daftar" backgroundColor="#55acee" />
            </View>
        );
    }
}
export default SignUpScreen;

const styles = new StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    }
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
        width: '80%',
    },
});