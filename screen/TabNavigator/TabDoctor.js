import React from 'react';
import { FlatList, Image, StyleSheet, Text, View } from 'react-native';
import { ListItem } from 'native-base';
import Odoo from 'react-native-odoo';

const odoo = new Odoo({
  host: '192.168.100.15',
  port: 8088,
  database: 'odoo',
  username: 'admin',
  password: 'admin'
});

// Connect to Odoo 
odoo.connect(function (err) {
  if (err) { return console.log(err); }
})

// var params = {
//   ids: [1,2,3,4,5],
//   fields: [ 'name' ],
// };
// odoo.get('res.partner', params, function (err, partners) {
//   if (err) { return console.log(err); }

//   console.log(partners);
// });
export default class App extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      partners: [{ name: 'Tidak Connect' }]
    }
  }
  componentDidMount() {
    // var Doctor = true;
    // Doctor = "is A Doctor?";
    odoo.search_read('res.users', {
      fields: ['name'],
      // domain: ['doctor', '=', Doctor]
    }, function (err, partners) {
      console.log(partners);
      this.setState({
        partners: partners
      })
    }.bind(this))
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>List Doctor</Text>
        <FlatList
          data={this.state.partners}
          renderItem={({ item }) => <Text>{item.name}</Text>}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
