import React from 'react';
import { Alert, Text, TextInput, StyleSheet, View, Button } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker'
import Odoo from 'react-native-odoo';

const odoo = new Odoo({
    host: '192.168.100.15',
    port: 8088,
    database: 'odoo',
    username: 'admin',
    password: 'admin'
})

odoo.connect(function (err) {
    if (err) { return console.log(err); }
})

export default class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = { partners: [{ name: 'Tidak Connect' }] }

        this.state = { date: "2016-05-15" };

        this.inputRefs = {};

        this.state = {
            favColor: undefined,
            items: [
                {
                    label: 'Poli Umum',
                    value: 'umum',
                },
                {
                    label: 'Poli Gigi',
                    value: 'gigi',
                },
                {
                    label: 'Poli Anak',
                    value: 'anak',
                },
                {
                    label: 'Poli Kulit',
                    value: 'kulit',
                },
            ],
            favDoctor: undefined,
            items3: [
                {
                    label: 'Doctor A',
                    value: 'a',
                },
                {
                    label: 'Doctor B',
                    value: 'b',
                },
                {
                    label: 'Doctor C',
                    value: 'b',
                },
            ],
        };
    }

    componentDidMount() {
        // if the component is using the optional `value` prop, the parent
        // has the abililty to both set the initial value and also update it
        setTimeout(() => {
            this.setState({
                favColor: 'Umum',
            });
        }, 1000);

        // parent can also update the `items` prop
        setTimeout(() => {
            this.setState({
                items: this.state.items.concat([{ value: 'ortu', label: 'Poli Orang Tua' }]),
            });
        }, 2000);
    }

    daftar() {
        var get = {
            fields: ['name', 'partner_id', 'date','clinic_id','doctor_id'],
        }
        odoo.search_read('clinic.appt', get, function (err, partners) {
            console.log(partners);
            this.setState({
                partners: partners
            })
        }.bind(this))
    }
    render() {
        return (
            <View style={styles.container}>
                <Text style={{ fontSize: 30, alignItems: 'center' }}>Silahkan Isi Data Dibawah Dengan Benar</Text>
                <Text>Nama</Text>
                <TextInput
                    placeholder="Nama Lengkap"
                    ref={(el) => {
                        this.inputRefs.name = el;
                    }}
                    returnKeyType="next"
                    enablesReturnKeyAutomatically
                    onSubmitEditing={() => {
                        this.inputRefs.picker.togglePicker();
                    }}
                    style={pickerSelectStyles.inputIOS}
                    blurOnSubmit={false}
                />

                <View style={{ paddingVertical: 5 }} />

                <Text>Pilih Poli</Text>
                <RNPickerSelect
                    placeholder={{
                        label: '--PILIH--',
                        value: null,
                    }}
                    items={this.state.items}
                    onValueChange={(value) => {
                        this.setState({
                            favColor: value,
                        });
                    }}
                    onUpArrow={() => {
                        this.inputRefs.name.focus();
                    }}
                    onDownArrow={() => {
                        this.inputRefs.picker2.togglePicker();
                    }}
                    style={{ ...pickerSelectStyles }}
                    value={this.state.favColor}
                    ref={(el) => {
                        this.inputRefs.picker = el;
                    }}
                />

                <View style={{ paddingVertical: 5 }} />

                <Text>Doctor</Text>
                <RNPickerSelect
                    placeholder={{
                        label: '--PILIH--',
                        value: null,
                    }}
                    items={this.state.items3}
                    onValueChange={(value) => {
                        this.setState({
                            favDoctor: value,
                        });
                    }}
                    onUpArrow={() => {
                        this.inputRefs.picker.togglePicker();
                    }}
                    onDownArrow={() => {
                        this.inputRefs.company.focus();
                    }}
                    style={{ ...pickerSelectStyles }}
                    value={this.state.favDoctor}
                    ref={(el) => {
                        this.inputRefs.picker2 = el;
                    }}
                />

                <View style={{ paddingVertical: 5 }} />

                <Text>Alamat</Text>
                <TextInput
                    placeholder="Masukan Alamat Anda"
                    ref={(el) => {
                        this.inputRefs.company = el;
                    }}
                    returnKeyType="go"
                    enablesReturnKeyAutomatically
                    style={pickerSelectStyles.inputIOS}
                    onSubmitEditing={() => {
                        Alert.alert('Success', 'Form submitted', [{ text: 'Okay', onPress: null }]);
                    }}
                />

                <View style={{ paddingVertical: 5 }} />

                <Text>Tentukan Jadwal</Text>
                <DatePicker
                    style={{ width: 420, }}
                    date={this.state.date}
                    mode="datetime"
                    placeholder="Masukan Tanggal"
                    format="YYYY-MM-DD HH:mm:ss"
                    minDate="2017-05-01"
                    maxDate="2025-12-29"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 4,
                            marginLeft: 0
                        },
                        dateInput: {
                            marginLeft: 36
                        },
                        placeholderText: {
                            fontSize: 18,
                        }
                    }}
                    onDateChange={(date) => { this.setState({ date: date }) }}
                />

                <View style={{ paddingVertical: 5 }} />

                <Button title="Submit" onPress={this.daftar.bind(this)}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 30,
        backgroundColor: '#fff',
        justifyContent: 'center',
        paddingHorizontal: 10,
    },
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
    },
});