import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { createDrawerNavigator } from 'react-navigation';
import HomeScreen from './HomeScreen';
import App from './JadwalScreen'


const AppDrawerNavigator = createDrawerNavigator({
    Home: { screen: HomeScreen },
    Jadwal: { screen: App },
},
);

export default AppDrawerNavigator;

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    drawerImage: {
        height: 150,
        width: 150,
        borderRadius: 75,
    }
});