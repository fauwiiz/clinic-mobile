import React, { Component } from 'react';
import { StyleSheet, Text, View, Linking, Alert } from 'react-native';
import { Button } from 'react-native-elements';
import Expo from 'expo';

class WelcomeScreen extends Component {

    async logIn() {
        const { type, token } = await Expo.Facebook.logInWithReadPermissionsAsync('1172289986260001', {
            permissions: ['public_profile'],
          });
        if (type === 'success') {
          // Get the user's name using Facebook's Graph API
          const response = await fetch(
            `https://graph.facebook.com/me?access_token=${token}`);
          Alert.alert(
            'Logged in!',
            `Hi ${(await response.json()).name}!`,
          );
        }
        if (response.type === 'success'){
            this.props.navigation.navigate('Home');
        }else{
            return {cencelled: true};
        }
      }

      async signInWithGoogleAsync() {
        try {
          const result = await Expo.Google.logInAsync({
            androidClientId: '492523639439-5qli3ujm0e9vvk0e8akh8mnt8dvd1rrq.apps.googleusercontent.com',
            iosClientId: '492523639439-vr0lhv59bbefjpjucosv6jbuu0ivbkgo.apps.googleusercontent.com',
            scopes: ['profile', 'email'],
          });
  
          if (result.type === 'success') {
            this.props.navigation.navigate('Home');
            return result.accessToken;
          } else {
            return {cancelled: true};
          }
        } catch(e) {
          return {error: true};
        }
      }
      
    render() {
        return (
            <View style={styles.container}>
                <Button containerViewStyle={{ width: '100%', paddingHorizontal: 10 }} backgroundColor="#55acee" title="Masuk" onPress={() => this.props.navigation.navigate('LoginScreen')} />
                <View style={{ paddingVertical: 5 }} />
                <Button containerViewStyle={{ width: '100%', paddingHorizontal: 10 }} backgroundColor="#55acee" title="Daftar" onPress={() => this.props.navigation.navigate('SignUpScreen')} />
                <View style={{ paddingVertical: 10 }} />
                <Button onPress={this.logIn.bind(this)} containerViewStyle={{ width: '100%', paddingHorizontal: 10, }} backgroundColor="#3b5998" title="Masuk dengan Facebook" />
                <View style={{ paddingVertical: 5 }} />
                <Button onPress={this.signInWithGoogleAsync.bind(this)} containerViewStyle={{ width: '100%', paddingHorizontal: 10 }} backgroundColor="#B22222" title="Masuk dengan Gmail" />
            </View>
        );
    }
}
export default WelcomeScreen;

const styles = new StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',

    }
});
