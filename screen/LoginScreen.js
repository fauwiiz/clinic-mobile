import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, Alert } from 'react-native';
import { Button } from 'react-native-elements';
import Odoo from 'react-native-odoo';
import { Actions } from 'react-native-router-flux';

const odoo = new Odoo({
    host: '192.168.100.15',
    port: 8088,
    database: 'odoo',
    username: 'admin',
    password: 'admin'
})

odoo.connect(function (err) {
    if (err) { return console.log(err); }
})
class LoginScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            partners: [{ name: 'Tidak Connect' }],
            user: null,
            pass: null
        }
    }

    login() {
        var get ={
            fields: ['login','password']
        }
        odoo.search_read('res.users', get, function (err, partners,user,pass) {
            if (user === 'login'){
                if(pass === 'password'){
                    this.props.navigation.navigate('DrawerNavigator');
                }
                this.props.navigation.navigate('DrawerNavigator');            
            }else{
                Alert.alert('Login Gagal')
        }console.log(partners);
        this.setState({
            partners: partners,
            user: user,
            pass: pass,
        })
        }.bind(this))
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>Username</Text>
                <TextInput onChangeText={(user) => this.setState({user})} value={this.state.user} style={pickerSelectStyles.inputIOS} />
                <View style={{ paddingVertical: 5 }} />
                <Text>Password</Text>
                <TextInput onChangeText={(pass) => this.setState({pass})} value={this.state.pass} secureTextEntry={true} password={true} style={pickerSelectStyles.inputIOS} />
                <View style={{ paddingVertical: 5 }} />
                <Button onPress={this.login.bind(this)} containerViewStyle={{ width: '70%', paddingHorizontal: 10 }} backgroundColor="#55acee" title="Lanjut Masuk" />
            </View>
        );
    }
}
export default LoginScreen;

const styles = new StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
    }
});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
        width: '80%',
    },
});