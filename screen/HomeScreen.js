import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import HomeStackTabNavigator from './HomeStackTabNavigator';
import { createStackNavigator } from 'react-navigation';

class HomeScreen extends Component{

    render(){
        return(
            <InnerSatckNavigator/>
        );
    }
}
export default HomeScreen;

const InnerSatckNavigator = createStackNavigator({
    HomeStackTabNavigator: { screen: HomeStackTabNavigator },
});

const styles = new StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});