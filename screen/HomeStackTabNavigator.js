import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Button } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
import TabAppointment from './TabNavigator/TabAppointment';
import TabDoctor from './TabNavigator/TabDoctor';
import { Entypo, FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons';

const ScreenTabNavigaor = createBottomTabNavigator({
  Appoinment: {
    screen: TabAppointment,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <MaterialCommunityIcons name="calendar" size={24} color={tintColor} />
      )
    }
  },
  Doctor: {
    screen: TabDoctor,
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <FontAwesome name="user-md" size={24} color={tintColor} />
      )
    }
  },
})

class Header extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitleStyle: { alignSelf: 'center', },
      title: 'CLINIC ABON',
      headerLeft: (
        <View style={{ padding: 10 }} >
          <Entypo name="menu" size={32} color="black" onPress={() => navigation.navigate('DrawerOpen')} />
        </View>
      ),
      headerRight: (
        <View style={{ padding: 10 }} >
          <FontAwesome name="search" size={30} color="black" onPress={() => navigation.navigate('DrawerOpen')} />
        </View>
      ),
    }
  }

  render() {
    return (
      <ScreenTabNavigaor screenProps={{ navigation: this.props.navigation }} />
    );
  }
}
export default Header;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})